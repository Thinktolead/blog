---
title: Michael Edited This
date: 2020-05-27
tags: Testing
---

It helps to relieve stress by keeping you engaged in something you enjoy. 
Hobbies give you a way to take your mind off the stresses of everyday life. 
They let you relax and seek pleasure in activities that aren't associated with work, chores or other responsibilities. 
Hobbies help you become more patient

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

You probably want to delete it and write your own articles!
